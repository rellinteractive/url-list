const Apify = require('apify')
const request = require('request')
const rp = require('request-promise')
const cheerio = require('cheerio')
const NodeGeocoder = require('node-geocoder')

var options = {
  provider: 'google',
  apiKey: 'AIzaSyCwrmRSz8al60ddXPiKOHYtb_RCUFlPdgE',
}

var geocoder = NodeGeocoder(options)

Apify.main(async () => {
  const source = 'https://stackoverflow.com/jobs?dr=FrontendDeveloper&sort=p'
  const webhookUrl =
    'https://us-central1-jobstack-a3981.cloudfunctions.net/addJob'

  // Get input of the act
  const input = await Apify.getValue('INPUT')
  if (!input || typeof input.items !== 'object') {
    throw new Error("Invalid input, it needs to contain 'items' object.")
  }

  // Create initial payload object
  const payload = {
    source,
    lastUpdated: parseInt((new Date().getTime() / 1000).toFixed(0)),
    items: [],
  }

  const formattedSourceResults = []
  const items = input.items

  items.forEach((item) => {
    const newItem = {
      datePosted: item.published,
      categories: item.categories,
      companyName: item.actor.displayName,
      jobUrl: item.permalinkUrl.replace(/\?.*$/, ''),
    }

    formattedSourceResults.push(newItem)
  })

  // Retrieve location details from geocoding

  function findLocationData(address, country) {
    return new Promise(function(resolve, reject) {
      return geocoder.geocode({ address, countryCode: country }, function(
        err,
        res,
      ) {
        if (err) {
          console.log('Error finding location data: ', err)
          reject(err)
        } else {
          // console.log('findLocationData: ', res)
          const newRes = res.map((data) => ({
            formattedAddress: data.formattedAddress,
            city: data.city,
            country: data.country,
            countryCode: data.countryCode,
            _geoloc: {
              lat: data.latitude,
              lng: data.longitude,
            },
          }))
          resolve(newRes)
        }
      })
    })
  }

  // Setup Request Promise to fetch Page data

  function requestPageData(result) {
    const url = result.jobUrl

    const optionsCheerio = {
      uri: url,
      headers: {
        'User-Agent':
          'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36',
      },
      transform: (body) => cheerio.load(body),
    }
    // Load the web page and extract meta-data
    console.log(`Opening ${url}`)

    return rp(optionsCheerio)
      .then(($) => {
        const geocodingLocations = []
        const jobBenefits = []
        let jobLocation = []
        const jsonLdData = JSON.parse(
          $('script[type="application/ld+json"]').html(),
        )
        console.log('jsonLdData: ', jsonLdData)
        if (jsonLdData) {
          if (jsonLdData.jobBenefits) {
            jsonLdData.jobBenefits.forEach(function(jobBenefit) {
              if (jobBenefit) jobBenefits.push(jobBenefit)
            })
          }
          if (jsonLdData.jobLocation) {
            jobLocation = jsonLdData.jobLocation.slice(0, 5).map((location) => {
              if (location) {
                const streetAddress = location.address.streetAddress || ''
                const addressLocality = location.address.addressLocality || ''
                const addressCountry = location.address.addressCountry || ''
                console.log(
                  `Address: ${streetAddress}, ${addressLocality}, ${addressCountry}`,
                )
                if (!streetAddress && !addressLocality) {
                  return {
                    countryCode: addressCountry,
                  }
                } else {
                  const address = `${streetAddress} ${addressLocality}`.trim()
                  geocodingLocations.push(
                    findLocationData(address, addressCountry),
                  )
                  // console.log('Current Locations Promise: ', geocodingLocations)
                  return geocodingLocations
                }
              }
            })
            console.log('Locations with All Promises: ', geocodingLocations)
            console.log('Locations length: ', geocodingLocations.length)
          }

          return Promise.all(geocodingLocations)
            .then((res) => {
              if (res.length > 0) {
                res = res.reduce((prev, curr) => prev.concat(curr))
              }

              geocodingLocations.length > 0 ? (jobLocation = res) : -1
              console.log('locationPromises res:', res)
              console.log('jobLocation:', jobLocation)

              const newResult = {
                title: jsonLdData.title || '',
                description: jsonLdData.description || '',
                employmentType: jsonLdData.employmentType || '',
                jobBenefits: jobBenefits,
                experienceRequirements: jsonLdData.experienceRequirements || '',
                companyUrl: jsonLdData.hiringOrganization.sameAs || '',
                companyLogo: jsonLdData.hiringOrganization.logo || '',
                jobLocation: jobLocation,
                remoteFriendly: $('.job-details--header .-remote').length
                  ? true
                  : false,
                ...result,
              }

              payload.items.push(newResult)
              console.log('Pushed to payload: ', payload.items.length)

              return newResult
            })
            .catch((err) => {
              console.log('locationPromises error:', err)
              return err
            })

          // return newResult
        } else {
          console.log("No ld-json data so don't crawl this page. Skip.")
          return
        }
      })
      .catch((err) => {
        console.log('error: ', err)
        return err
      })
  }

  async function urlPromises(array) {
    const promises = array.map(requestPageData)

    return await Promise.all(promises)
  }

  const crawlData = await urlPromises(formattedSourceResults)

  if (crawlData) {
    if (payload.items.length > 0) {
      console.log('Posting data to webhook:', webhookUrl)
      const postWebhook = await rp({
        method: 'POST',
        uri: webhookUrl,
        body: payload,
        json: true,
      })
    } else {
      console.log('Payload has no items to post: ', payload.items.length)
    }
  }

  console.log('crawlData: ', crawlData)

  console.log('end')
  await Apify.setValue('OUTPUT', crawlData)
})
